package com.dassader.servlet;

import com.dassader.util.FileExplorer;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * @author Andrii_Kulikov
 */
public class FileServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String path = req.getParameter("path");
        String command = req.getParameter("command");

        if(path == null) {
            resp.getWriter().write("Please enter parameter 'path'");
            return;
        }

        File file = new File(path);
        if(file.isFile()) {
            if(command != null) {

            } else {
                new Thread() {
                    @Override
                    public void run() {
                        CommandLine cmdLine = new CommandLine("cmd.exe");
                        cmdLine.addArgument("/c");
                        cmdLine.addArgument(path);
                        DefaultExecutor executor = new DefaultExecutor();
                        try {
                            executor.execute(cmdLine);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
            return;
        }

        FileExplorer fileExplorer = new FileExplorer(path);
        resp.getWriter().write(fileExplorer.getJSONFileList().toString());
    }
}
