import com.dassader.server.MyServer;
import com.dassader.servlet.FileServlet;
import com.dassader.servlet.GetSystemTimeServlet;
import com.dassader.servlet.HelloServlet;

import java.awt.*;
import java.net.URI;

/**
 * @author Andrii_Kulikov
 */
public class RunMe {
    public static void main(String[] args) throws Exception {
        MyServer myServer = new MyServer(80);

        myServer.addContext(HelloServlet.class, "/");
        myServer.addContext(GetSystemTimeServlet.class, "/getTime");
        myServer.addContext(FileServlet.class, "/file");

        myServer.start();

        Desktop.getDesktop().browse(new URI("http://localhost"));
    }
}